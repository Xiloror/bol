class 'Plugin'

local Axes = {}
--Axes info
local AxesObjectName = "Draven_Q_reticle_self.troy"
local AxeRadius = 100
local AxeRadius2 = AxeRadius - 20
local AxeDuration = 2000 --2 seconds until the axe disapears

local lastGen = 0
local Skills, Keys, Items, Data, Jungle, Helper, MyHero, Minions, Crosshair, Orbwalker = AutoCarry.Helper:GetClasses()

Menu = AutoCarry.Plugins:RegisterPlugin(Plugin(), "Draven") 
Menu:addParam("Catch0", "Catch axes in AC mode", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("Catch1", "Catch axes in mixed mode", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("Catch2", "Catch axes in lashit mode", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("Catch3", "Catch axes in laneclear mode", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("Generate", "Generate random axes for testing", SCRIPT_PARAM_ONOFF, false)
Menu:addParam("CatchRange", "Dont catch if distance > (from mouse)", SCRIPT_PARAM_SLICE, 1000, 0, 3000, 0)

Menu:addParam("Drawing", "Drawing", SCRIPT_PARAM_INFO, "")
Menu:addParam("DrawAxes", "Draw circle arround axes", SCRIPT_PARAM_ONOFF, true)


function Plugin:__init()
	AdvancedCallback:bind('OnGainBuff', OnGainbuff)
	AdvancedCallback:bind('OnLoseBuff', OnLosebuff)
end

function Plugin:OnTick()
	RemoveOldAxes()
	if Menu.Generate and (GetTickCount() - lastGen) > AxeDuration then
		Plugin.OnCreateObj({name = AxesObjectName, x= myHero.x + math.random(-600, 600) , z= myHero.z + math.random(-600, 600) })
	end
	
 	local CurrentAxe = GetNextAxe()
	if CurrentAxe then
		local AxePosition = Vector(CurrentAxe.position.x, 0, CurrentAxe.position.z)
		local MyPos = Vector(myHero.x, 0, myHero.z)
		local MPos = Vector(mousePos.x, 0, mousePos.z)
		
		AutoCarry.CanMove = false --Take control over movement
		
		if GetDistance(AxePosition) > AxeRadius then
			local MovePoint = AxePosition + AxeRadius2 * (MyPos - AxePositon):normalized()
		else
			
			if GetDistance(AxePosition, MPos) <= AxeRadius2 then
				local MovePoint = MPos
			else
				--[[TODO: This point should be the intersection between axe circle and the line between MPos and MyPos]]
				local MovePoint = AxePosition + AxeRadius2 * (MPos - AxePositon):normalized()
			end
		end
		MoveToPoint(Point)
	else
		AutoCarry.CanMove = true --Let autocarry handle the movement
	end
end

local function MoveToPoint(Point)
	if (Orbalker.stage == AutoCarry.STAGE_MOVE) and not Helper:IsEvading() then --We are not attacking/Evading stuff
		if GetDistance(Point) > 50 then
			myHero:MoveTo(Point.x, Point.z)
		end
	end
end

local function GetNextAxe()
	if (Keys.AutoCarry and Menu.Catch0) or (Keys.MixedMode and Menu.Catch1) or (Keys.LastHit and Menu.Catch2) or (Keys.LaneClear and Menu.Catch3) then
		for _, Axe in ipairs(Axes) do
			--[[Check if the axe is too far from mouse]]
			if GetDistance(Vector(Axe.position.x, 0, Axe.position.z)) < Menu.CatchRange then
				return Axe
			end
		end
	elseRemoveOldReticles
		return nil
	end
	return nil
end

local function RemoveOldAxes()
	local i=1
	while i <= #Axes do
	    if  GetTickCount() - Axes[i].time > AxeDuration then
		table.remove(input, i)
	    else
		i = i + 1
	    end
	end
end

function Plugin:OnDraw()
	if Menu.DrawAxes then
		for _, Axe in ipairs(Axes) do
			Helper:DrawCircleObject(Axe, AxeRadius, ARGB(255, 0, 255, 0), 1)
		end
	end
end

function Plugin:OnCreateObj(object)
	--[[New axe]]
	if object.valid and object.name == AxesObjectName then
		table.insert(Axes, {time = GetTickCount(), position = Vector(object.x, 0, object.z)})
	end
end

--[[TODO: Manage buff stacks]]
local function OnGainbuff()

end

local function OnGainbuff()

end
